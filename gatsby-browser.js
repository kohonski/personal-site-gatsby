import '@fortawesome/fontawesome-svg-core/styles.css';
import { config } from '@fortawesome/fontawesome-svg-core';
import './src/pages/index.scss';
import 'prismjs/themes/prism-okaidia.css';

config.autoAddCss = false;
