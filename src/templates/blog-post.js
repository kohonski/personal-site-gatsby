import React from 'react';
import { Link, graphql } from 'gatsby';

import BlogLayout from '../components/BlogLayout/blog-layout';
import styles from './BlogPost.module.scss';
import Helmet from 'react-helmet';

class BlogPostTemplate extends React.Component {
  render() {
    const data = this.props.data;

    const post = data.markdownRemark;
    const siteTitle = data.site.siteMetadata.siteTitle;
    const { previous, next } = this.props.pageContext;

    const blogPrefix = data.site.siteMetadata.subroutePaths.blog;
    const pageTitle = `${post.frontmatter.title} | ${data.site.siteMetadata.title}`;
    return (
      <BlogLayout location={this.props.location} title={siteTitle}>
        <Helmet>
          <title>{pageTitle}</title>
          <meta name="title" content={pageTitle} />
          <meta name="description" content={post.frontmatter.description} />
        </Helmet>
        <article className={styles.blogArticle}>
          <header>
            <h1 className={styles.titleHeading}>{post.frontmatter.title}</h1>
            <p>{post.frontmatter.date}</p>
          </header>
          <section dangerouslySetInnerHTML={{ __html: post.html }} />
          <hr />
        </article>
        <nav>
          <ul className={styles.navList}>
            <li>
              {previous && (
                <Link
                  className={styles.adjacentArticleLink}
                  to={blogPrefix + previous.fields.slug}
                  rel="prev"
                >
                  ← {previous.frontmatter.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link
                  className={styles.adjacentArticleLink}
                  to={blogPrefix + next.fields.slug}
                  rel="next"
                >
                  {next.frontmatter.title} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
      </BlogLayout>
    );
  }
}
export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
        subroutePaths {
          blog
        }
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
      }
    }
  }
`;
