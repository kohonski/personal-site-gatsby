import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faGithub,
  faGitlab,
  faLinkedin
} from '@fortawesome/free-brands-svg-icons';
library.add(faGithub);
