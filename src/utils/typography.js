import Typography from 'typography';

const typography = new Typography({
  baseFontSize: '16px',
  baseLineHeight: 1.666,
  headerFontFamily: ['Lato', 'serif'],
  bodyFontFamily: ['Lato', 'serif'],
  includeNormalize: true,
  overrideStyles: ({ adjustFontSizeTo, scale, rythm }, options) => ({
    'p code': {
      fontSize: '1rem'
    },
    'h1 code, h2 code, h3 code, h4 code, h5 code, h6 code': {
      fontSize: 'inherit'
    },
    'code li ': {
      fontSize: '1rem'
    },
    img: {
      fontSize: '0.75rem'
    }
  })
});

// Hot reload typography in development
if (process.env.NODE_ENV !== `production`) {
  typography.injectStyles();
}

export default typography;
export const rhythm = typography.rhythm;
export const scale = typography.scale;
