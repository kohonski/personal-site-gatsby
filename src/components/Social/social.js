import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { StaticQuery, graphql } from 'gatsby';
import {
  faGithub,
  faGitlab,
  faLinkedin
} from '@fortawesome/free-brands-svg-icons';

import styles from './Social.module.scss';

const Social = ({ siteMetadata }) => {
  return (
    <div className={styles.container}>
      <a
        href={siteMetadata.gitlabHandle}
        target="_blank"
        rel="noopener noreferrer"
        className={styles.socialIconLink}
      >
        {' '}
        <FontAwesomeIcon icon={faGitlab} />
      </a>
      <a
        href={siteMetadata.githubHandle}
        target="_blank"
        rel="noopener noreferrer"
        className={styles.socialIconLink}
      >
        {' '}
        <FontAwesomeIcon icon={faGithub} />
      </a>
      <a
        href={siteMetadata.linkedinHandle}
        target="_blank"
        rel="noopener noreferrer"
        className={styles.socialIconLink}
      >
        <FontAwesomeIcon icon={faLinkedin} />
      </a>
    </div>
  );
};

export default () => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            githubHandle
            gitlabHandle
            linkedinHandle
          }
        }
      }
    `}
    render={data => <Social siteMetadata={data.site.siteMetadata} />}
  />
);
