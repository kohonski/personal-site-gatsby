import React from 'react';
import { Link } from 'gatsby';
import styles from './BlogLayout.module.scss';
import Layout from '..//Layout/layout';

class BlogLayout extends React.Component {
  render() {
    const { location, title, children } = this.props;
    const rootPath = `${__PATH_PREFIX__}/blog/`;
    let header;

    if (location.pathname === rootPath) {
      header = (
        <h1 linkHeader>
          <Link className={styles.link} to={`/`}>
            {title}
          </Link>
        </h1>
      );
    } else {
      header = (
        <h3 className={styles.linkHeader}>
          <Link className={styles.link} to={`/`}>
            {title}
          </Link>
        </h3>
      );
    }
    return (
      <Layout>
        <div className={styles.container}>
          <header>{header}</header>
          <main>{children}</main>
          <footer>
            <small>© {new Date().getFullYear()} Nic Biddell</small>
          </footer>
        </div>
      </Layout>
    );
  }
}

export default BlogLayout;
