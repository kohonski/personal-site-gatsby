import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import HeaderNav from '@components/HeaderNav/header-nav';
import Helmet from 'react-helmet';
import styles from './Layout.module.scss';

export default ({ children }) => {
  const data = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `
  );
  const siteMetadata = data.site.siteMetadata;
  return (
    <div className={styles.pageWrapper}>
      <Helmet>
        <title>{siteMetadata.title}</title>
        <meta name="title" content={siteMetadata.title} />
        <meta name="description" content={siteMetadata.description} />
      </Helmet>
      <HeaderNav />
      <div className={styles.contentContainer}>{children}</div>
    </div>
  );
};
