import React from 'react';

import styles from './Intro.module.scss';

export default () => {
  return (
    <div className="intro">
      <h1 className={styles.heroText}>
        Hi, I'm <span className={styles.boldText}>Nic Biddell</span>, a
        full-stack web developer with a focus on API development, Docker, & am
        an AWS Certified Solutions Architect
      </h1>
    </div>
  );
};
