import React from 'react';
import { StaticQuery, Link, graphql } from 'gatsby';

import styles from './HeaderNav.module.scss';

const HeaderNav = ({ siteMetadata }) => {
  const blogPath = siteMetadata.subroutePaths.blog;
  return (
    <nav>
      <ul className={styles.navList}>
        <li>
          <Link className={styles.link} activeClassName={styles.active} to="/">
            Home
          </Link>
        </li>
        <li>
          <Link
            className={styles.link}
            activeClassName={styles.active}
            to={blogPath}
          >
            Blog
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default () => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            title
            subroutePaths {
              blog
            }
          }
        }
      }
    `}
    render={data => <HeaderNav siteMetadata={data.site.siteMetadata} />}
  />
);
