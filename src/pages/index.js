import React from 'react';

import Layout from '@components/Layout/layout';
import Intro from '@components/Intro/intro';
import Social from '@components/Social/social';
import styles from './Index.module.scss';

export default () => (
  <Layout>
    <div className={styles.contentContainer}>
      <Intro />
      <Social />
    </div>
  </Layout>
);
