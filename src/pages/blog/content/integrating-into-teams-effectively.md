---
date: "2020-04-20"
title: "Integrating with new teams effectively"
description: "Reflections after several team and project switches."
publish: false
---

Sketch:

- Aim to understand the business needs and constraints just as much as the tech product
- Ask thoughtful questions
- Make proposals when asking for advice and feedback
- Find opportunities for building up your teammates
- Don't "Rage against the codebase"

### Aim to understand the business needs and constraints just as much as the tech product
### Ask thoughtful questions
### Make proposals when asking for advice and feedback
### Find opportunities for building up your teammates
### Don't "Rage against the codebase"
