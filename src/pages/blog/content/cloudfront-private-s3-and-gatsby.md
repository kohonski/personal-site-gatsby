---
date: "2020-07-27"
title: "Securing S3 for Gatsby with Cloudfront"
description: "Want to make your bucket completely private and serve everything through Cloudfront? Cool! But server-side redirects won't work out of the box... Let's address this with a Lambda@Edge!"
publish: true
---

_Update 10/14/20: I open-sourced the module and published it on the Terraform Registry! Check it out [here](https://registry.terraform.io/modules/biddellns/static-site/aws/latest)!_

I'm in the process of serving this site on AWS's CDN. Since this is built with Gatsby, there's a bit of a pain point. As of writing, the current [Gatsby solution for S3/Cloudfront](https://www.gatsbyjs.org/docs/deploying-to-s3-cloudfront/) leaves your bucket open for read. I'd much rather lock down my bucket and force all requests to go through Cloudfront.

## A bit of context

Gatsby creates routes like _/blog/_ or _/blog/cloudfront-private-s3-and-gatsby/_. When your browser makes a request, Gatsby's framework - or in my case, Nginx - can recognize that as a directory and append index.html to the end of those links. So we'd get _/blog/index.html_ and _/blog/cloudfront-private-s3-and-gatsby/index.html_ respectively.

## So what's the issue?
Cloudfront can't be configured to serve a default directory/index object other than root. This means we can access _mysite.com_ and _mysite.com/_ just fine! But if we try to go to _mysite.com/blog/_, you're going to get an error. 

Quoting the Gatsby solution link above (emphasis mine) :
> _There are two ways that you can connect CloudFront to an S3 origin. The most obvious way, which the AWS Console will suggest, is to type the bucket name in the Origin Domain Name field. This sets up an S3 origin, and allows you to configure CloudFront to use IAM to access your bucket. Unfortunately, it also makes it impossible to perform serverside (301/302) redirects, and it also means that directory indexes (having index.html be served when someone tries to access a directory) will only work in the root directory. You might not initially notice these issues, because Gatsby’s clientside JavaScript compensates for the latter and plugins such as gatsby-plugin-meta-redirect can compensate for the former. **But just because you can’t see these issues, doesn’t mean they won’t affect search engines.**_

## Well... why not just serve through S3?
My requirements are:

1. Site served over CDN
2. Resources served over HTTPS
3. Keep bucket permissions air-tight

Req #1 pretty much leads us to Cloudfront. :) 
Req #2 is interesting because S3 can't serve objects over HTTPS. You have to have it pass through Cloudfront.

Since we'll have to use Cloudfront, I'd really like to lock my bucket down so it can ONLY be accessed by Cloudfront.

You'd also have to use Cloudfront if you're in a spot where you want to restrict content to certain parties (e.g., authenticated users). And for that to work properly you **must** lock down public access to your bucket.

## Solution time

We'll need:
- A Cloudfront Distribution
- An S3 bucket
  - All public access blocked
- A bucket policy allowing an [Origin Access Identity](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html#private-content-creating-oai) access to the bucket.
- A [Lamba@Edge](https://aws.amazon.com/lambda/edge/) to handle requests

Because I'm a masochist, I'm going to write this out in [Terraform](https://www.terraform.io/). 

Just kidding! 

I've been learning it on the side and I really like having infrastructure as code. It's much easier to track why/how somthing changed. It's also much easier to figure out what a known good-state looked like, if I did make a mistake.

I'll be using [variables](https://www.terraform.io/docs/configuration/variables.html) like `var.bucket_name` and `var.cert_arn`, but you can hardcode these if you like.

#### S3 bucket
For the bucket, we'll keep things simple and create the bucket, then block all public access.

```hcl
resource "aws_s3_bucket" "static_site" {
  acl    = "private"
  bucket = var.bucket_name
}

resource "aws_s3_bucket_public_access_block" "static_site" {
  bucket = aws_s3_bucket.static_site.id

  block_public_acls = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}
```

Okay cool! Now nobody can access the bucket!

#### Cloudfront config

Next, we need to create a Cloudfront Distribution, and point it to our S3 bucket. 
I added some stuff for aliases and SSL certs, but that's optional depending on your use-case.

```hcl
resource "aws_cloudfront_distribution" "cdn" {
  enabled = true

  # Optional, but needed if you have a domain name you want to use!
  aliases = var.cloudfront_cname_aliases

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods = ["GET", "HEAD"]
    compress = true
    target_origin_id = "S3-${aws_s3_bucket.static_site.bucket_regional_domain_name}"

    # If you don't create a certificate, you'll likely want 'allow-all' instead
    viewer_protocol_policy = "redirect-to-https"
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    default_ttl = var.default_cache_time
    max_ttl = var.default_cache_time
    min_ttl = 0
  }
  default_root_object = var.default_root_object
  is_ipv6_enabled = true
  origin {
    domain_name = aws_s3_bucket.static_site.bucket_regional_domain_name
    origin_id = "S3-${aws_s3_bucket.static_site.bucket_regional_domain_name}"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  # Leave this empty if you don't have a certificate. I highly reccomend you make one if you're on AWS anyways. It's free up to 1,000 certs!
  # https://docs.aws.amazon.com/acm/latest/userguide/acm-overview.html
  viewer_certificate {
    acm_certificate_arn = var.cert_arn
    minimum_protocol_version = "TLSv1.2_2019"
    ssl_support_method = "sni-only"
  }
}
```

If we try to access pages through our CDN's URL, we're going to get 403s. That's because we need to create an [Origin Access Identity](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-restricting-access-to-s3.html#private-content-creating-oai) so explicit access permissions are set between the Cloudfront and S3 bucket.

An Origin Access Identity (OAI) is a special user and we'll only allow that user to access files in the S3 bucket. 

First, let's create the OAI.
```hcl
resource "aws_cloudfront_origin_access_identity" "static_site_s3" {
    comment = "static_site_s3"
}
```

Going back to the Cloudfront config, let's add the OAI to the origin:

```hcl
origin {
    domain_name = aws_s3_bucket.static_site.bucket_regional_domain_name
    origin_id = "S3-${aws_s3_bucket.static_site.bucket_regional_domain_name}"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.static_site_s3.cloudfront_access_identity_path
    }
  }
```

One more step is we need to add a bucket policy, since all permissions are locked down on the S3 side. This will allow our Origin Access Identity to read from the bucket:

```hcl
resource "aws_s3_bucket_policy" "static_site" {
  bucket = aws_s3_bucket.static_site.id
  policy = data.aws_iam_policy_document.static_site.json
}

data "aws_iam_policy_document" "static_site" {
  statement {
    actions   = ["s3:GetObject"]
    resources = [
      "${aws_s3_bucket.static_site.arn}/*"
    ]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.static_site_s3.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.static_site.arn]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.static_site_s3.iam_arn]
    }
  }
}
```

Whew! We added quite a bit and our CDN is ready to go! If we access our site's root page, we'll be met with success!

But what if we try to access a route that looks like a subdirectory? Unfortunately, we'd get a failure as it's currently set up

## Time for the secret sauce

We need to add a lambda at the edge to modify the request a bit. Cloudfront uses lambdas to modify requests and responses. If you're familiar with Apache or nginx, you've likely created rules to catch certain requests patterns.

Here's an example from the [Nginx docs](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/#root):
```nginx
location / {
    root /data;
    index index.html index.php;
}
```

So if you went to _example.com/_, Nginx would do an internal redirect to serve /data/index.html from the webserver. Lambdas are the AWS way to set up rules like these.

Here's the terraform to create our Lambda:
```hcl
resource "aws_lambda_function" "index_html" {
  filename = data.archive_file.dummy.output_path
  function_name = "index-html-writer"
  handler = "exports.handler"
  role = aws_iam_role.lambda_edge_exec.arn
  runtime = "nodejs12.x"
  publish = true
}

resource "aws_iam_role" "lambda_edge_exec" {
  assume_role_policy = data.aws_iam_policy_document.lambda-at-edge.json
}

data "aws_iam_policy_document" "lambda-at-edge" {
  statement {
    actions   = ["sts:AssumeRole"]
    effect = "Allow"
    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
        "edgelambda.amazonaws.com"
      ]
    }
  }
}

data "archive_file" "dummy" {
  output_path = "${path.module}/lambda_function_code.zip"
  type = "zip"

  source {
    content = "console.log('Hello World');"
    filename = "exports.js"
  }
}
```

Why do we have a default "dummy" archive file? Because you can't create a Lambda in terraform without a source file. We could have pointed to an actual dummy file as well, but I chose to put it all in terraform. You won't have this issue if you go directly through the console.

Now that we have a Lambda created, we'll need to attatch it as a behavior.

This means whenever you hit the default route (e.g., every request), the lambda will read (and potentially) modify the request.

In our last code block, we had a source, with a content and filename key. Since we defined the filename as exports.js, we'll need to create that file in our lambda. Here's the code I added to exports.js:
```javascript
exports.handler = (event, context, callback) => {
  const request = event.Records[0].cf.request;
  const uri = request.uri;

  if (uri.endsWith('/')) {
    request.uri += 'index.html';
  } else if (!uri.includes('.')) {
    request.uri += '/index.html';
  }

  callback(null, request);
};
```

This is good to go! Now we just need to wire it up to our cloudfront.

We'll need to pick a an _event type_ or trigger, for our lambda. 

AWS has a list of [best-practices](https://aws.amazon.com/blogs/networking-and-content-delivery/lambdaedge-design-best-practices/) and a handy image:
![Graphic showing request and response-types](./../assets/L@E.png)

There's also a list of criteria to help determine what event type to use. We know we want to modify a request, so should it be a _Viewer Request_ or an _Origin Request_? They have a few answers that stood out to me:

- Do you want to generate a response that can be cached? Use an origin request trigger.
- Do you want to rewrite a URL to the origin? Use an origin request trigger.
- Do you want your function to be executed on a cache miss? Use origin triggers.
- Do you want your function to be executed on all requests? Use viewer triggers.

The answer to the first three is yes! And we don't want to run this lambda on _every_ request since we're caching the results anyways, so... Origin Request it is!

Let's adjust our Cloudfront's default cache behavior:
```hcl
  default_cache_behavior {
    allowed_methods = ["GET", "HEAD", "OPTIONS"]
    cached_methods = ["GET", "HEAD"]
    compress = true
    target_origin_id = "S3-${aws_s3_bucket.static_site.bucket_regional_domain_name}"
    viewer_protocol_policy = "redirect-to-https"
    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = aws_lambda_function.index_html.qualified_arn
    }

    default_ttl = var.default_cache_time
    max_ttl = var.default_cache_time
    min_ttl = 0
  }
```

And that's it! Subdirectory routes work now! And our S3 bucket is sealed off from public access!