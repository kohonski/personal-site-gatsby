---
date: "2020-03-30"
title: "Why write a tech blog?"
description: "Here are some reasons that matter to me."
publish: true
---

My blog is in a working state with Gatsby! I went in and out of focus over 6 months but finally did it!

This is something I've wanted to do for a long time. I've realized a a few things as I've started my career: 
- I still enjoy programming as a hobby on my own time
- I miss out on sharing the cool things when learning stuff at home
- I consume a lot of content, but don't produce content

The first two points play hand in hand. I like playing with different tech/languages, and there's lots of subtle (or not) paradigms and philosophies along the journey. Angular vs Vue vs React. Java & OO patterns vs Go's procedural and simplistic design. What does packaging and dependency management look across ecosystems?  What does getting to production look like? What's the deal with Elixir/Erlang and how they handle failure? I've recently been dabbling in Rust and listening to related talks. That's taken me to C++ [talks by Chandler Carruth](https://www.youtube.com/watch?v=fHNmRkzxHWs) and performance considerations that invalidated lots of what I thought I understood. 

There's an overwhelming amount of things that are beneficial to learn so I've generally been content to read/listen and soak up little bits without worrying too much. I also limit time on side projects as that's tended to leave me exhausted for work the next day when do over-do it. But there are times I learn something really neat that I wish I could've shared. Sometimes it's not always relevant to my immediate network of co-workers but might be useful to people in a particular stack or ecosystem.

Here's an example! At home I was working on a small [Django](https://www.djangoproject.com/) project. The goal was to upload a CSV to a website, validate the CSV, and display the information back to the user. I broke it out to an Angular client and used Django for the REST API. It occured to me at this point that I'd never done file uploads with REST before. So that was new. Django also doesn't (or didn't) "do" REST APIs. There was however, a de-facto, 3rd party library at the time. Enter the [Django REST Framework](https://www.django-rest-framework.org/). Cool! I'd learn how to upload files to a REST API **and** I'd learn a new framework! 

Django is super cool in that a lot of functionality is based off your model. Once you define your model, the ORM handles all the database stuff. The Views (think http handlers in this instance) are also really easy to hook into. It's magical and building stuff out is a realy fast process!

My model was literally just it's data, and the time of upload.
```python
class CsvUpload(models.Model):
    document = models.FileField(validators=[import_csv_file_validator])
    upload_date = models.DateField(auto_now=True)
```

The HTTP handler abstractions were a lot harder to figure out. You can use a [ModelViewSet](https://www.django-rest-framework.org/api-guide/viewsets/#modelviewset) that creates HTTP handlers for your  model. I won't post the doc snippet here but it is very thin. It didn't click for me at the time and it took a lot of stack overflow.

```python
class CsvUploadViewSet(viewsets.ModelViewSet):
    queryset = CsvUpload.objects.all()
    serializer_class = CsvUploadSerializer
    parser_classes = (MultiPartParser,)

    def create(self, request):
        try:
            file_obj = request.FILES['upload']
            serializer = CsvUploadSerializer(data={'document':file_obj})
            serializer.is_valid()
            serializer.save()

            return Response(serializer.data, status=HTTP_201_CREATED)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)

    @detail_route(methods=['get'])
    def get_data(self, request, pk=None):
        document = self.get_object()
        filename = document.document

        data = convert_csv_to_json(filename)

        return Response(data=data, status=200)
```

This is what I came up with! I had to piece together a lot of docs and stackoverflow. I won't labor through the code details here but do compare this to the scant ModelViewSet documentation. Now remember, at the time, I worked as a Full-stack Java/Angular engineer.  My team at work was heavily invested in our stack and Django/Python would not have been a good fit. The biggest thing I learned during this project was an implementation concern, so it wasn't relevant if you weren't trying to use Django as a RESTful service.

That was a time ago, but that feeling still sticks with me. Moving forward, I want a place to explain and document how I did stuff. Much like I'd share with co-workers. It's certainly something I'd want to do fresh and not from 3 years afterwards. So now I have an outlet to explain things as I go in detail and that's a better move forward.

### Consumption and Production
I believe it's also important to balance input with output. Anecdotally, I've always learned faster, with better understanding when I'm involved in what I'm learning. I had this experience with public speaking when I signed up for a two-day training. I presented several times, got feedback, and watched a classroom full of others do the same. Now, whenever I listen to a presentation, I'm able to absorb more detail. How do they keep energy flowing? How do they walk? How do they handle pauses? How do they handle distractions, questions, a talkative attendee? How do they build their hooks and closers? Where are they looking throughout their presentation?

I'd never have gotten that perspective if I hadn't focused on that skill. I certainly haven't focused on writing and breaking down complex ideas enough yet. This blog will be fine arena to practice in. :)