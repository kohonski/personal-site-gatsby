import React from 'react';
import { Link, graphql } from 'gatsby';

import Layout from '../../components/BlogLayout/blog-layout';
import styles from './Index.module.scss';
import Helmet from 'react-helmet';

const blogIndex = ({ data, location }) => {
  // Just found out this was wrong. Keeping wrong for now to make header nav okay
  const siteTitle = data.site.siteMetadata.siteTitle;
  const posts = data.allMarkdownRemark.edges;
  const blogPrefix = data.site.siteMetadata.subroutePaths.blog;

  const pageTitle = `Blog | ${data.site.siteMetadata.title}`;

  return (
    <Layout location={location} title={siteTitle}>
      <Helmet>
        <title>{pageTitle}</title>
        <meta name="title" content={pageTitle} />
      </Helmet>
      {posts.map(({ node }) => {
        const title = node.frontmatter.title || node.fields.slug;
        const pathToPost = `${blogPrefix + node.fields.slug}`;
        return (
          <article key={node.fields.slug}>
            <header>
              <h3 className={styles.titleHeader}>
                <Link className={styles.link} to={pathToPost}>
                  {title}
                </Link>
              </h3>
              <small>{node.frontmatter.date}</small>
            </header>
            <section>
              <p
                dangerouslySetInnerHTML={{
                  __html: node.frontmatter.description || node.excerpt
                }}
              />
            </section>
          </article>
        );
      })}
    </Layout>
  );
};

export default blogIndex;

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        subroutePaths {
          blog
        }
      }
    }
    allMarkdownRemark(
      filter: { frontmatter: { publish: { eq: true } } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
          }
        }
      }
    }
  }
`;
